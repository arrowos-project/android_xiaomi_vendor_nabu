# Automatically generated file. DO NOT MODIFY
#
# This file is generated by device/xiaomi/nabu/setup-makefiles.sh

PRODUCT_SOONG_NAMESPACES += \
    vendor/xiaomi/nabu

PRODUCT_COPY_FILES += \
    vendor/xiaomi/nabu/proprietary/etc/permissions/com.motorola.frameworks.core.addon.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.motorola.frameworks.core.addon.xml \
    vendor/xiaomi/nabu/proprietary/etc/permissions/moto-settings.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/moto-settings.xml \
    vendor/xiaomi/nabu/proprietary/lib/libcamera_algoup_jni.xiaomi.so:$(TARGET_COPY_OUT_SYSTEM)/lib/libcamera_algoup_jni.xiaomi.so \
    vendor/xiaomi/nabu/proprietary/lib64/libcamera_algoup_jni.xiaomi.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libcamera_algoup_jni.xiaomi.so \
    vendor/xiaomi/nabu/proprietary/lib64/libcameraservice.so:$(TARGET_COPY_OUT_SYSTEM)/lib64/libcameraservice.so \
    vendor/xiaomi/nabu/proprietary/product/etc/permissions/com.motorola.dolby.dolbyui.dax3.features.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.motorola.dolby.dolbyui.dax3.features.xml \
    vendor/xiaomi/nabu/proprietary/system/etc/permissions/privapp-permissions-qti.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-qti.xml \
    vendor/xiaomi/nabu/proprietary/product/etc/sysconfig/hiddenapi-whitelist-com.motorola.dolby.dolbyui.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/hiddenapi-whitelist-com.motorola.dolby.dolbyui.xml \
    vendor/xiaomi/nabu/proprietary/system/etc/sysconfig/qti_whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/qti_whitelist.xml \
    vendor/xiaomi/nabu/proprietary/system_ext/etc/permissions/com.android.hotwordenrollment.common.util.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.android.hotwordenrollment.common.util.xml \
    vendor/xiaomi/nabu/proprietary/system_ext/etc/permissions/com.dolby.daxservice.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.dolby.daxservice.xml \
    vendor/xiaomi/nabu/proprietary/system_ext/etc/permissions/privapp-permissions-qti-system-ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-qti-system-ext.xml \
    vendor/xiaomi/nabu/proprietary/system_ext/etc/permissions/qti_libpermissions.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/qti_libpermissions.xml \
    vendor/xiaomi/nabu/proprietary/system_ext/etc/permissions/qti_permissions.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/qti_permissions.xml \
    vendor/xiaomi/nabu/proprietary/system_ext/lib/com.qualcomm.qti.ant@1.0.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib/com.qualcomm.qti.ant@1.0.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib/libmmosal.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib/libmmosal.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib/libmmparser_lite.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib/libmmparser_lite.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib/libqdMetaData.system.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib/libqdMetaData.system.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib/vendor.display.color@1.0.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib/vendor.display.color@1.0.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib/vendor.display.color@1.1.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib/vendor.display.color@1.1.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib/vendor.display.color@1.2.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib/vendor.display.color@1.2.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib/vendor.display.color@1.3.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib/vendor.display.color@1.3.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib64/com.qualcomm.qti.ant@1.0.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64/com.qualcomm.qti.ant@1.0.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib64/libmmosal.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64/libmmosal.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib64/libmmparser_lite.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64/libmmparser_lite.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib64/libqdMetaData.system.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64/libqdMetaData.system.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib64/vendor.display.color@1.0.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64/vendor.display.color@1.0.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib64/vendor.display.color@1.1.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64/vendor.display.color@1.1.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib64/vendor.display.color@1.2.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64/vendor.display.color@1.2.so \
    vendor/xiaomi/nabu/proprietary/system_ext/lib64/vendor.display.color@1.3.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64/vendor.display.color@1.3.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/bin/hw/vendor.dolby.hardware.dms@2.0-service:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/bin/hw/vendor.dolby.hardware.dms@2.0-service \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/etc/init/vendor.dolby.hardware.dms@2.0-service.rc:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/etc/init/vendor.dolby.hardware.dms@2.0-service.rc \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/etc/media_codecs_dolby_audio.xml:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/etc/media_codecs_dolby_audio.xml \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib/libdapparamstorage.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib/libdapparamstorage.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib/libdeccfg.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib/libdeccfg.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib/libstagefright_soft_ac4dec.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib/libstagefright_soft_ac4dec.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib/libstagefright_soft_ddpdec.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib/libstagefright_soft_ddpdec.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib/libstagefrightdolby.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib/libstagefrightdolby.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib/soundfx/libswdap.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib/soundfx/libswdap.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib/soundfx/libswgamedap.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib/soundfx/libswgamedap.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib/soundfx/libvolumelistener.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib/soundfx/libvolumelistener.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib/vendor.dolby.hardware.dms@2.0.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib/vendor.dolby.hardware.dms@2.0.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/libcamera_dirty.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/libcamera_dirty.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/libdapparamstorage.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/libdapparamstorage.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/libdeccfg.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/libdeccfg.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/libdlbdsservice.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/libdlbdsservice.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/libstagefright_soft_ac4dec.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/libstagefright_soft_ac4dec.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/libstagefright_soft_ddpdec.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/libstagefright_soft_ddpdec.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/libstagefrightdolby.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/libstagefrightdolby.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/soundfx/libswdap.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/soundfx/libswdap.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/soundfx/libswgamedap.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/soundfx/libswgamedap.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/soundfx/libvolumelistener.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/soundfx/libvolumelistener.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/vendor.dolby.hardware.dms@2.0-impl.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/vendor.dolby.hardware.dms@2.0-impl.so \
    vendor/xiaomi/nabu/proprietary/product/vendor_overlay/30/lib64/vendor.dolby.hardware.dms@2.0.so:$(TARGET_COPY_OUT_PRODUCT)/vendor_overlay/30/lib64/vendor.dolby.hardware.dms@2.0.so

PRODUCT_PACKAGES += \
    MiuiCamera \
    PowerOffAlarm \
    MotoDolbyDax3 \
    HotwordEnrollmentOKGoogleHEXAGON \
    HotwordEnrollmentXGoogleHEXAGON \
    MotorolaSettingsProvider \
    daxService \
    com.motorola.frameworks.core.addon \
    moto-checkin \
    moto-settings \
    motoaudioeffectsdk \
    com.android.hotwordenrollment.common.util
